package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.BitmapFont;


public class World {
	private Player1 player1;
	private Player2 player2;
	private Player3 player3;
	private Block block;
	private GameTime gameTime;
	public int numberOfPlayer;
    private InvisibleMazeBattleGame invisibleMazeBatteGame;
    World(InvisibleMazeBattleGame invisibleMazeBatteGame , int numberOfPlayer) {
        this.invisibleMazeBatteGame = invisibleMazeBatteGame;
        this.numberOfPlayer = numberOfPlayer;
        block = new Block();
        block.changeMapToChar();
        gameTime = new GameTime(this);
        player1 = new Player1(block.getSpawnX(),block.getSpawnY(),this);
        player2 = new Player2(block.getSpawnX(),block.getSpawnY(),this);
        if(numberOfPlayer == 3){
        	player3 = new Player3(block.getSpawnX(),block.getSpawnY(),this);
        }
    }
    
    public void update(float delta) {
    	updateInputDirectionPlayer1();
    	player1.update();
    	updateInputDirectionPlayer2();
    	player2.update();
    	if(numberOfPlayer == 3){
    		updateInputDirectionPlayer3();
    		player3.update();
    	}
    }
    
    private void updateInputDirectionPlayer1() {
		if(Gdx.input.isKeyPressed(Keys.W)) {
			player1.setNextDirection(Player1.DIRECTION_UP);
        }
		else if(Gdx.input.isKeyPressed(Keys.A)) {
			player1.setNextDirection(Player1.DIRECTION_LEFT);
        }
		else if(Gdx.input.isKeyPressed(Keys.S)) {
			player1.setNextDirection(Player1.DIRECTION_DOWN);
        } 
		else if(Gdx.input.isKeyPressed(Keys.D)) {
			player1.setNextDirection(Player1.DIRECTION_RIGHT);
        }
		else{
			player1.setNextDirection(Player1.DIRECTION_STILL);
		}
	}
    
    private void updateInputDirectionPlayer2() {
		if(Gdx.input.isKeyPressed(Keys.O)) {
			player2.setNextDirection(Player2.DIRECTION_UP);
        }
		else if(Gdx.input.isKeyPressed(Keys.K)) {
			player2.setNextDirection(Player2.DIRECTION_LEFT);
        }
		else if(Gdx.input.isKeyPressed(Keys.L)) {
			player2.setNextDirection(Player2.DIRECTION_DOWN);
        } 
		else if(Gdx.input.isKeyPressed(Keys.SEMICOLON)) {
			player2.setNextDirection(Player2.DIRECTION_RIGHT);
        }
		else{
			player2.setNextDirection(Player2.DIRECTION_STILL);
		}
	}
    
    private void updateInputDirectionPlayer3() {
		if(Gdx.input.isKeyPressed(Keys.NUMPAD_8)) {
			player3.setNextDirection(Player1.DIRECTION_UP);
        }
		else if(Gdx.input.isKeyPressed(Keys.NUMPAD_4)) {
			player3.setNextDirection(Player1.DIRECTION_LEFT);
        }
		else if(Gdx.input.isKeyPressed(Keys.NUMPAD_5)) {
			player3.setNextDirection(Player1.DIRECTION_DOWN);
        } 
		else if(Gdx.input.isKeyPressed(Keys.NUMPAD_6)) {
			player3.setNextDirection(Player1.DIRECTION_RIGHT);
        }
		else{
			player3.setNextDirection(Player1.DIRECTION_STILL);
		}
	}
    
    Player1 getPlayer1(){
    	return player1;
    }
    
    Player2 getPlayer2(){
    	return player2;
    }
    
    Player3 getPlayer3(){
    	return player3;
    }
    
    Block getBlock() {
        return block;
    }
    
    public void endGame(String message){
//    	EndGameScreen.setVisible(true);
	 invisibleMazeBatteGame.setScreen(new EndGameScreen(invisibleMazeBatteGame, message));
    }
    
    GameTime getGameTime(){
    	return gameTime;
    }
}