package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class WorldRenderer {
	private InvisibleMazeBattleGame invisibleMazeBatteGame;
	World world;
	private Block block;
	private GameTime gameTime;
	private EffectSkill effectSkill;
	public SpriteBatch batch;
	private Texture background;
	public BlockRenderer blockRenderer;
	public PlayerRenderer playerRenderer;
	public static final int BLOCK_SIZE = 40;
	
	public WorldRenderer(InvisibleMazeBattleGame invisibleMazeBatteGame , World world) {
		this.invisibleMazeBatteGame = invisibleMazeBatteGame;
		batch = invisibleMazeBatteGame.batch;
		blockRenderer = new BlockRenderer(invisibleMazeBatteGame.batch, world);
		playerRenderer = new PlayerRenderer(invisibleMazeBatteGame.batch, world);
		effectSkill = new EffectSkill(invisibleMazeBatteGame.batch, world);
		this.world = world;
		block = world.getBlock();
		gameTime = world.getGameTime();
		background = new Texture("BackGround3.png");
	}
	
	 public void render(float delta) {
		 SpriteBatch batch = invisibleMazeBatteGame.batch;
		 batch.begin();
		 gameTime.render();
		 batch.draw(background, 0, 0);
		 blockRenderer.render();
//		 effectSkill.render();
		 playerRenderer.render();
//		 effectSkill.render();
	     batch.end();
	 }

}
