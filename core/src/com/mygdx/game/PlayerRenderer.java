package com.mygdx.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class PlayerRenderer {
	private SpriteBatch batch;
	private Player1 player1;
	private Player2 player2;
	private Player3 player3;
	private Texture redWizardImg;
	private Texture blueWizardImg;
	private Texture greenWizardImg;
	private Block block;
	World world;
	public BitmapFont font;
    private int scale = 3;
	
	public PlayerRenderer(SpriteBatch batch, World world) {
		this.batch = batch;
		this.world = world;
		font = new BitmapFont();
        font.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
        font.getData().setScale(scale);
        redWizardImg = new Texture("RedWizard.png");
		blueWizardImg = new Texture("BlueWizard.png");
		greenWizardImg = new Texture("GreenWizard.png");
		block = world.getBlock();
		player1 = world.getPlayer1();
		player2 = world.getPlayer2();
		if(world.numberOfPlayer == 3){
        	player3 = world.getPlayer3();
        }
	}

	 public void render() {
		 Vector2 pos1 = world.getPlayer1().getPosition();
		 Vector2 pos2 = world.getPlayer2().getPosition();
		 if(world.numberOfPlayer == 2){
	    	 font.setColor(Color.RED);
		     font.draw(batch , "Player1" , block.getWidth()*WorldRenderer.BLOCK_SIZE/20 , (float)InvisibleMazeBattleGame.HEIGHT - ((float)((float)(block.getHeightOfMenu()+1)*(float)WorldRenderer.BLOCK_SIZE)/2)+ WorldRenderer.BLOCK_SIZE);
		     font.setColor(Color.BLUE);
		     font.draw(batch , "Player2" , block.getWidth()*10*WorldRenderer.BLOCK_SIZE/20 , (float)InvisibleMazeBattleGame.HEIGHT - ((float)((float)(block.getHeightOfMenu()+1)*(float)WorldRenderer.BLOCK_SIZE)/2)+ WorldRenderer.BLOCK_SIZE);
	     }else if(world.numberOfPlayer == 3){
	    	 Vector2 pos3 = world.getPlayer3().getPosition();
	    	 batch.draw(greenWizardImg, pos3.x - WorldRenderer.BLOCK_SIZE/2, InvisibleMazeBattleGame.HEIGHT - pos3.y - WorldRenderer.BLOCK_SIZE/2);
	    	 font.setColor(Color.RED);
		     font.draw(batch , "Player1" , block.getWidth()*WorldRenderer.BLOCK_SIZE/24 , (float)InvisibleMazeBattleGame.HEIGHT - ((float)((float)(block.getHeightOfMenu()+1)*(float)WorldRenderer.BLOCK_SIZE)/2)+ WorldRenderer.BLOCK_SIZE);
		     font.setColor(Color.BLUE);
		     font.draw(batch , "Player2" , block.getWidth()*8*WorldRenderer.BLOCK_SIZE/24 , (float)InvisibleMazeBattleGame.HEIGHT - ((float)((float)(block.getHeightOfMenu()+1)*(float)WorldRenderer.BLOCK_SIZE)/2)+ WorldRenderer.BLOCK_SIZE);
		     font.setColor(Color.GREEN);
		     font.draw(batch , "Player3" , block.getWidth()*15*WorldRenderer.BLOCK_SIZE/24 , (float)InvisibleMazeBattleGame.HEIGHT - ((float)((float)(block.getHeightOfMenu()+1)*(float)WorldRenderer.BLOCK_SIZE)/2)+ WorldRenderer.BLOCK_SIZE);
	     }
	     batch.draw(blueWizardImg, pos2.x - WorldRenderer.BLOCK_SIZE/2, InvisibleMazeBattleGame.HEIGHT - pos2.y - WorldRenderer.BLOCK_SIZE/2);
	     batch.draw(redWizardImg, pos1.x - WorldRenderer.BLOCK_SIZE/2, InvisibleMazeBattleGame.HEIGHT - pos1.y - WorldRenderer.BLOCK_SIZE/2);
	 }
}
