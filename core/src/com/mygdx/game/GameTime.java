package com.mygdx.game;

import com.badlogic.gdx.Gdx;

public class GameTime {
	private float time=0;
	private World world;
	public boolean timeRun = true;
	public GameTime(World world) {
		this.world = world;
	}
	public void render () {
		if(timeRun){
			time +=Gdx.graphics.getDeltaTime();
		}
//	    System.out.println(time);
	}
	
	public float returnTime(){
		return time;
	}
	
}
