package com.mygdx.game;



import java.util.LinkedList;

import java.util.List;



import com.badlogic.gdx.math.Vector2;



public class Player2 {

	private Vector2 position;
	private BlockRenderer blockRenderer;
    private int currentDirection;
    private int nextDirection;
    private World world;
    private int newRow;
    private int newCol;
    private PlayerCommand playerCommand;
    
    public static final int DIRECTION_UP = PlayerCommand.DIRECTION_UP;
    public static final int DIRECTION_RIGHT = PlayerCommand.DIRECTION_RIGHT;
    public static final int DIRECTION_DOWN = PlayerCommand.DIRECTION_DOWN;
    public static final int DIRECTION_LEFT = PlayerCommand.DIRECTION_LEFT;
    public static final int DIRECTION_STILL = PlayerCommand.DIRECTION_STILL;
   
	public Player2(int x, int y, World world){
		playerCommand = new PlayerCommand(x,y,world,"Player 2");
		this.world = world;
//		this.blockRenderer = WorldRenderer.blockRenderer;
	}

	public Vector2 getPosition(){
		return playerCommand.getPosition();
	}

	public void move(int dir){
		playerCommand.move(dir);
	}

    public void setNextDirection(int dir) {
    	playerCommand.setNextDirection(dir);
    }  

    public void update() {

    	playerCommand.update();
    }

    public boolean isAtCenter() {
        return playerCommand.isAtCenter();
    }

    private boolean canMoveInDirection(int dir){
    	return playerCommand.canMoveInDirection(dir);
    }
    
    private int getRow(){
    	return playerCommand.getRow();
    }

    private int getColumn(){
    	return playerCommand.getColumn();
    }

    
}