package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class BlockRenderer {
	private Block block;
    private SpriteBatch batch;
    private Texture blockImage;
    private Texture textE;
    private Texture textX;
    private Texture textI;
    private Texture textT;
    
    public BlockRenderer(SpriteBatch batch, World world) {
        this.block = world.getBlock();
        this.batch = batch;
        blockImage = new Texture("BlockInGame2.png");
        textE = new Texture("TextE.png");
        textX = new Texture("TextX.png");
        textI = new Texture("TextI.png");
        textT = new Texture("TextT.png");
        }
    
    public void render() {        
        for(int r = 0; r < block.getHeight() + block.getHeightOfMenu() ; r++) {
            for(int c = 0; c < block.getWidth(); c++) {
                int x = c * WorldRenderer.BLOCK_SIZE;
                int y = InvisibleMazeBattleGame.HEIGHT -(r * WorldRenderer.BLOCK_SIZE) - WorldRenderer.BLOCK_SIZE;
                
                if(block.hasBlockAtDraw(r, c)) {
                    batch.draw(blockImage, x, y);
                }
                
                if(block.checkWord(r, c,'e')||block.checkWord(r, c,'E') || block.checkWord(r, c,'A')) {
                    batch.draw(textE, x, y);
                }else if(block.checkWord(r, c,'x')||block.checkWord(r, c,'X') || block.checkWord(r, c,'B')) {
                    batch.draw(textX, x, y);
                }else if(block.checkWord(r, c,'i')||block.checkWord(r, c,'I') || block.checkWord(r, c,'C')) {
                    batch.draw(textI, x, y);
                }else if(block.checkWord(r, c,'t')||block.checkWord(r, c,'T') || block.checkWord(r, c,'D')) {
                    batch.draw(textT, x, y);
                }
            }
        }
    }

}