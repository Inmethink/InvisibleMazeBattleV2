package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;


public class EndGameScreen extends ScreenAdapter {
	public BitmapFont font;
	private InvisibleMazeBattleGame invisibleMazeBatteGame;
	private Player1 player;
	private WorldRenderer worldRenderer;
	String message;
	private float fontX;
	private float fontY;
	
	
	public EndGameScreen(InvisibleMazeBattleGame invisibleMazeBatteGame,String player ) {
        this.invisibleMazeBatteGame = invisibleMazeBatteGame;
        font = new BitmapFont();
        font.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
        font.getData().setScale(5f);
        message = player + "   Win!!!";
        final GlyphLayout layout = new GlyphLayout(font, message);
        fontX = (InvisibleMazeBattleGame.WIDTH - layout.width) / 2;
        fontY = (InvisibleMazeBattleGame.HEIGHT - layout.height) / 2;     
    }
	
	
	 @Override
	 public void render(float delta) {
		 SpriteBatch batch  = invisibleMazeBatteGame.batch;
	     Gdx.gl.glClearColor(0, 0, 0, 1);
	     Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	     batch.begin();
	     font.draw(batch , message , fontX , fontY);
	     batch.end();
	 }
}