package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class StartScreen extends ScreenAdapter {
	Texture the2PlayerButton;
	Texture the3PlayerButton;
	public Rectangle button2Player;
	public Rectangle button3Player;
	private InvisibleMazeBattleGame invisibleMazeBatteGame;
	
	public StartScreen(InvisibleMazeBattleGame invisibleMazeBatteGame){
		this.invisibleMazeBatteGame = invisibleMazeBatteGame;
		button2Player = new Rectangle(InvisibleMazeBattleGame.WIDTH/2 - 200, InvisibleMazeBattleGame.HEIGHT*2/3, 400, 100);
		button3Player = new Rectangle(InvisibleMazeBattleGame.WIDTH/2 - 200, InvisibleMazeBattleGame.HEIGHT/3, 400, 100);
		the2PlayerButton = new Texture(Gdx.files.internal("2PlayerButton.png"));
		the3PlayerButton = new Texture(Gdx.files.internal("3PlayerButton.png"));
	}
	
	public void button1() {
		if(Gdx.input.justTouched())
//			System.out.println(Gdx.input.getY());
//			System.out.println(Gdx.graphics.getHeight());
//			System.out.println(InvisibleMazeBattleGame.HEIGHT);
//			System.out.println("-------------------------------");
//			Gdx.input.isTouched()
//			Gdx.input.isKeyJustPressed()
//			Gdx.input.isKeyPressed()
		if (button2Player.contains(Gdx.input.getX() * InvisibleMazeBattleGame.WIDTH/Gdx.graphics.getWidth() ,InvisibleMazeBattleGame.HEIGHT -  Gdx.input.getY()*InvisibleMazeBattleGame.HEIGHT/Gdx.graphics.getHeight())) {
			invisibleMazeBatteGame.setScreen(new GameScreen(invisibleMazeBatteGame, 2));
		}else if(button3Player.contains(Gdx.input.getX() * InvisibleMazeBattleGame.WIDTH/Gdx.graphics.getWidth(),InvisibleMazeBattleGame.HEIGHT -  Gdx.input.getY()*InvisibleMazeBattleGame.HEIGHT/Gdx.graphics.getHeight())){
			invisibleMazeBatteGame.setScreen(new GameScreen(invisibleMazeBatteGame, 3));
		}
	}
	
	@Override
	public void render(float delta) {
		SpriteBatch batch  = invisibleMazeBatteGame.batch;
		Gdx.gl.glClearColor(0, 0, 0, 1);
	    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		batch.draw(the2PlayerButton , button2Player.x, button2Player.y);
		batch.draw(the3PlayerButton , button3Player.x, button3Player.y);
		button1();
		batch.end();
	}
}
