package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;


public class GameScreen extends ScreenAdapter {
	private InvisibleMazeBattleGame invisibleMazeBatteGame;
	private Player1 player1;
	private Player2 player2;
	private Player3 player3;
	private WorldRenderer worldRenderer;
	World world;
	
	public GameScreen(InvisibleMazeBattleGame invisibleMazeBatteGame, int numberOfPlayer) {
        this.invisibleMazeBatteGame = invisibleMazeBatteGame;
        world = new World(invisibleMazeBatteGame, numberOfPlayer);
        player1 = world.getPlayer1();
        player2 = world.getPlayer2();
        worldRenderer = new WorldRenderer(invisibleMazeBatteGame , world);
        if(numberOfPlayer == 3){
        	player3 = world.getPlayer3();
        }
    }
	
	public void update(float delta){
		world.update(delta);
	}
	
	 @Override
	 public void render(float delta) {
		 SpriteBatch batch  = invisibleMazeBatteGame.batch;
		 world.update(delta);
		 update(delta);
	     Gdx.gl.glClearColor(0, 0, 0, 1);
	     Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	     worldRenderer.render(delta);
	 }
	 
}
