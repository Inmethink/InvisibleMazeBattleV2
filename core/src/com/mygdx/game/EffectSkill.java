package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class EffectSkill {
	private SpriteBatch batch;
	World world;
	private Texture [] redEffectSkill = new Texture [5];
	private Player1 player1;
	private Player2 player2;
	private Player3 player3;
	private GameTime gameTime;
	public boolean redStart = true;
	
	public EffectSkill(SpriteBatch batch , World world) {
		this.batch = batch;
		this.world = world;
		gameTime = world.getGameTime();
		redEffectSkill[0] = new Texture("RedEffectSkill1.png");
		redEffectSkill[1] = new Texture("RedEffectSkill2.png");
		redEffectSkill[2] = new Texture("RedEffectSkill3.png");
		redEffectSkill[3] = new Texture("RedEffectSkill4.png");
		redEffectSkill[4] = new Texture("RedEffectSkill5.png");
		player1 = world.getPlayer1();
	}
	
	public void render(){
		Vector2 pos1 = world.getPlayer1().getPosition();
		if(player1.getPlayerCommand().skillCanUse()){
			if(redStart){
				for(int i = 0; i < 2 ;i ++){
					batch.draw(redEffectSkill[i], pos1.x - WorldRenderer.BLOCK_SIZE/2, InvisibleMazeBattleGame.HEIGHT - pos1.y - WorldRenderer.BLOCK_SIZE/2);
				}
			}
			if(gameTime.returnTime()%1 ==0){
			for(int i = 2; i < 5 ;i ++){
				batch.draw(redEffectSkill[i], pos1.x - WorldRenderer.BLOCK_SIZE/2, InvisibleMazeBattleGame.HEIGHT - pos1.y - WorldRenderer.BLOCK_SIZE/2);
				}
			}
			redStart = false;
		}
	}
}
